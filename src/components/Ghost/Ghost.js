import React, { Component } from 'react'

import './style.css'
import { ReactComponent as GhostSVG} from './ghost.svg'

class Ghost extends Component {

  state = {
    direction: 'right',
    position: {
      top: 150,
      left: 150
    }
  }

  componentDidMount() {
    console.log('dupa')
    this.changeDirectionInterval = setInterval(this.changeDirection, 1000)
    this.moveInterval = setInterval(this.move, 360)
  }

  componentWillUnmount () {
    clearInterval(this.changeDirectionInterval)
    clearInterval(this.moveInterval)
  }

  changeDirection = () => {
    const movementArray = ['left', 'right', 'down', 'up']

    // TODO: ADD time uncertainity
    const movementDir = Math.floor(Math.random() * 4)

    setTimeout(()=>{
      this.setState({
        direction: movementArray[movementDir]
      })
    }, Math.floor(Math.random()*800))
  }

  move = () => {
    const currentTop = this.state.position.top
    const currentLeft = this.state.position.left
    const { direction } = this.state
    const { step, border, size, topScoreBoardHeight } = this.props

    // console.log(this.state.position)

    switch (direction) {
      case 'up':
        this.setState({
          position: {
            // top: currentTop - step,
            top: Math.max(currentTop - step, 0),
            left: currentLeft
          },
          direction: 'up'
        })
        break;
      case 'down':
        this.setState({
          position: {
            // top: currentTop + this.props.step,
            top: Math.min(currentTop + step, window.innerHeight - border - size - topScoreBoardHeight),
            left: currentLeft
          },
          direction: 'down'
        })
        break;
      case 'left':
        this.setState({
          position: {
            top: currentTop,
            // left: currentLeft - step
            left: Math.max(currentLeft - step, 0)
          },
          direction: 'left'
        })
        break;
      case 'right':
        this.setState({
          position: {
            top: currentTop,
            left: Math.min(currentLeft + step, window.innerWidth - border - size)
          },
          direction: 'right'
        })
        break;
        default:
    }
  }
  render () {
    const { color } = this.props
    const { position } = this.state

    return (
      <div 
        className={`ghost ghost-${color}`}
        style={position}
      >
        <GhostSVG />
      </div>
    )
  }
}

Ghost.defaultProps = {
  color: 'red',
  step: 50,
  size: 50,
  border: 10 * 2,
  topScoreBoardHeight: 50
}

export default Ghost